
declare var Promise: any
import framework from './framework'
import * as React from 'react'


 
export class ComponentClient<P, S> extends React.Component<P, S> {

    constructor (props) {
        super(props)
    }

    public emit(message: string, data: any, component?: string){
        //if (component) message = component + ":" + message
        framework.socket.emit(message, data)
    }
} 

