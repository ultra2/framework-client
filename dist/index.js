define("framework", ["require", "exports", "socket.io-client"], function (require, exports, io) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class FrameworkClient {
        constructor() {
            if (FrameworkClient._instance) {
                throw new Error("The Global is a singleton class and cannot be created!");
            }
            FrameworkClient._instance = this;
            this.components = {};
            this.socket = io.connect();
            var onevent = this.socket.onevent;
            this.socket.onevent = function (packet) {
                var args = packet.data || [];
                onevent.call(this, packet);
                packet.data = ["*"].concat(args);
                onevent.call(this, packet);
            };
            this.socket.on("*", function (message, data) {
                var splittedMessage = message.split(':');
                var component = splittedMessage[0];
                var method = splittedMessage[1];
                var componentInstance = this.components[component];
                if (componentInstance != null) {
                    componentInstance[method](data);
                }
            }.bind(this));
        }
        static getInstance() {
            return FrameworkClient._instance;
        }
    }
    FrameworkClient._instance = new FrameworkClient();
    exports.FrameworkClient = FrameworkClient;
    var g = FrameworkClient.getInstance();
    exports.default = g;
});
define("component", ["require", "exports", "framework", "react"], function (require, exports, framework_1, React) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    class ComponentClient extends React.Component {
        constructor(props) {
            super(props);
        }
        emit(message, data, component) {
            framework_1.default.socket.emit(message, data);
        }
    }
    exports.ComponentClient = ComponentClient;
});
