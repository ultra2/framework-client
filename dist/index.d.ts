/// <reference types="react" />
declare module "framework" {
    export class FrameworkClient {
        private static _instance;
        components: any;
        socket: any;
        constructor();
        static getInstance(): FrameworkClient;
    }
    var g: FrameworkClient;
    export default g;
}
declare module "component" {
    import * as React from 'react';
    export class ComponentClient<P, S> extends React.Component<P, S> {
        constructor(props: any);
        emit(message: string, data: any, component?: string): void;
    }
}
