import * as io from 'socket.io-client'



export class FrameworkClient {

    private static _instance: FrameworkClient = new FrameworkClient();
    public components: any
    public socket: any

    constructor() {
        if(FrameworkClient._instance) {
            throw new Error("The Global is a singleton class and cannot be created!");
        }

        FrameworkClient._instance = this;

        this.components = {}
        this.socket = io.connect()
        
        var onevent = this.socket.onevent
        this.socket.onevent = function (packet) {
            var args = packet.data || []
            onevent.call(this, packet)    // original call
            packet.data = ["*"].concat(args)
            onevent.call(this, packet)    // additional call to catch-all
        }
        
        this.socket.on("*",function(message, data) {
            var splittedMessage = message.split(':')
            var component = splittedMessage[0]
            var method = splittedMessage[1]

            var componentInstance = this.components[component]
            if (componentInstance != null){
                componentInstance[method](data)
            }
        }.bind(this))
    }

    public static getInstance() : FrameworkClient {
        return FrameworkClient._instance;
    }
}

var g = FrameworkClient.getInstance()
export default g





