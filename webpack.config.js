var path = require('path')

module.exports = {
    entry: "./dist/index.js",
    output: {
        path: path.resolve(__dirname, "bin"),
        filename: "bundle.js"
    },
    module: {
        loaders: [
            { test: /\.css$/, loader: "style!css" }
        ]
    }
};